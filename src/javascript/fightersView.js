import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import FighterDetailsView from './fighterDetailsView';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    let clickedFighterDetails;

    if (!this.fightersDetailsMap.has(fighter._id)) {
      clickedFighterDetails = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, clickedFighterDetails);

    } else {
      clickedFighterDetails = this.fightersDetailsMap.get(fighter._id);
    }

    new FighterDetailsView(clickedFighterDetails);
  }
}

export default FightersView;
