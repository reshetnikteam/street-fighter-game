import Fighter from './fighter';


let chosenFighters = [];

function acceptFighter(fighter){
    if (chosenFighters.length < 2){
        chosenFighters.push(new Fighter(fighter));

    } else {
        alert(`Two fighters were chosen. Let's fight!!!`);
        fight(chosenFighters);
    }
}

function fight(fighters){
    console.log('fight begin');

    let attacker = fighters[0];
    let defender = fighters[1];

    attack(attacker, defender);
    isWin(attacker, defender);
}


function attack(attacker, defender){
    let hitPoints;
    hitPoints = attacker.getHitPower() - defender.getBlockPower();
    defender.setHealthPoints(defender.getHealthPoints() - hitPoints);
    console.log('attack works');
}

function isWin(attacker, defender){
    if (defender.health <= 0){
        console.log(`${attacker.name} WIN`);

    } else {
        switchHitTurn();
        fight(chosenFighters);
    }
}

function switchHitTurn(){
    chosenFighters.push(chosenFighters.shift());
    console.log('switchTurn works');
}


export { acceptFighter}
