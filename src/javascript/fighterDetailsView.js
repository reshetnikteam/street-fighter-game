import View from './view';
import { acceptFighter } from './fight';

class FighterDetailsView extends View {
  constructor(clickedFighterDetails) {
    super();
    this.createModal(clickedFighterDetails);
  }

  static rootElement = document.getElementById('root');

  createModal(clickedFighterDetails) {
    const { name, source, health, attack, defense } = clickedFighterDetails;
    const nameElement = this.createNameElement(name);
    const imageElement = this.createImageElement(source);
    const healthElement = this.createHealthElement(health);
    const attackElement = this.createAttackElement(attack);
    const defenseElement = this.createDefenseElement(defense);
    const crossButton = this.createButtonCross();
    const acceptFighterButton = this.createAcceptButton();

    this.element = this.createElement({ tagName: 'div', className: 'modal' });
    const crossButtonSection = this.createElement({ tagName: 'div', className: 'cross-button-box' });
    const fighterDetailsSection = this.createElement({ tagName: 'div', className: 'fighter-detail-main-box'});

    const pictureSection = this.createElement({ tagName: 'div', className: 'picture-box' });
    const infoSection = this.createElement({ tagName: 'div', className: 'fighter-datails-box'});
    this.element.append(crossButtonSection, fighterDetailsSection);

    fighterDetailsSection.append(pictureSection, infoSection);
    crossButtonSection.append(crossButton);
    pictureSection.append(imageElement);
    infoSection.append(nameElement, healthElement, attackElement, defenseElement, acceptFighterButton);

    this.overlay = this.createElement({ tagName: 'div', className: 'overlay'});

    FighterDetailsView.rootElement.appendChild(this.element);
    FighterDetailsView.rootElement.appendChild(this.overlay);


    crossButton.addEventListener('click', () => this.destroyElement(this.element, this.overlay));
    acceptFighterButton.addEventListener('click', () => acceptFighter(clickedFighterDetails));
    this.overlay.addEventListener('click', () => this.destroyElement(this.element, this.overlay));

  }

  createNameElement(name) {
      const nameElement = this.createElement({
        tagName: 'div',
        className: 'fighter-name'
      });
      nameElement.innerText = name;
      return nameElement;
    }

  createImageElement(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-picture',
      attributes
    });
    return imgElement;
  }

  createHealthElement(health) {
    const healthElement = this.createElement({
      tagName: 'div',
      className: 'fighter-health',
    });
    healthElement.innerText = health;
    return healthElement;
  }

  createAttackElement(attack) {
    const attackElement = this.createElement({
      tagName: 'div',
      className: 'fighter-attack',
    });
    attackElement.innerText = attack;
    return attackElement;
  }

  createDefenseElement(defense) {
    const defenseElement = this.createElement({
      tagName: 'div',
      className: 'fighter-defense',
    });
    defenseElement.innerText = defense;
    return defenseElement;
  }

  createButtonCross() {
    const buttonCross = this.createElement({
      tagName: 'div',
      className: 'cross-button',
    });
    buttonCross.innerText = 'x';
    return buttonCross;
  }

  createAcceptButton() {
    const acceptButton = this.createElement({
      tagName: 'div',
      className: 'accept-button'
    });
    acceptButton.innerText = 'Accept FIGHTER!';
    return acceptButton;
  }
}

export default FighterDetailsView;
