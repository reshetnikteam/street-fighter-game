
class Fighter{
    constructor(fighter) {

        const { name, source, health, attack, defense } = fighter;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    getHitPower(){
        let hitPower = this.attack * randomChance();
        return hitPower;
    }

    getBlockPower(){
        let blockPower = this.defense * randomChance();
        return blockPower;
    }

    getHealthPoints(){
        return this.health;
    }

    setHealthPoints(healthPoints){
        this.health = healthPoints;
    }

}

     function randomChance(){
        return Math.floor(Math.random() * (3 - 1) + 1);
    }


export default Fighter
